﻿using System;

namespace TwoSumII
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] n = new int[]{2,7,11,15};
            int[] n1 = new int[]{-1, 0};

            int[] m = TwoSum(n, 18);
            int[] m1 = TwoSum(n1, -1);
            foreach (var item in m)
            {
                System.Console.Write(item + " ");
            }
            foreach (var item in m1)
            {
                System.Console.Write(item + " ");
            }

        }

        public static int[] TwoSum(int[] numbers, int target) {
            int[] indices = new int[2];
            indices[0] = -1;
            indices[1] = -1;
            

            if(numbers == null || numbers.Length == 0){
                return indices;
            }

            int left = 0;
            int right = numbers.Length - 1;

            while(left < right){
                int sum = numbers[left] + numbers[right];
                if(sum == target){
                    indices[0] = left + 1;
                    indices[1] = right + 1;
                    return indices;
                }else if(sum < target){
                    left++;
                }else{
                    right--;
                }

            }
            return indices;
        }
    }
}
